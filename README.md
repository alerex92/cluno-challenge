# Cluno challenge

This sample app contains an interpretation of Clean Architecture applied on Android.

There are three different packages:
* data: contains all of the implementation details of data accessing (like network APIs) 
* domain: contains "pure" models and interfaces
* presentation: contains all the view, fragments, ViewModel and UI models

The presentation layer is implemented using MVVM to achieve a more testable and maintainable structure.
In the data layer there is an implementation of the Repository pattern to abstract data sources.

The app uses a mix of RxJava and LiveData. The former is used until the ViewModel takes control of the flow and then exposes LiveData for the View layer (Fragments in this case).
Dagger 2 (Android) is used to handle dependency injection.

Mappers are used between the layers so that each component can access the appropriate type of model (e.g. network models, domain models or UI models).

Examples of unit and integration tests have been added to show the potential of the architecture and to cover some use cases (mainly regarding data parsing and conversion).