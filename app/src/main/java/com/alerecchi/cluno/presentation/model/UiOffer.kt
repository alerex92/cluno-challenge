package com.alerecchi.cluno.presentation.model

import com.alerecchi.cluno.domain.model.FuelType

data class UiOffer(
    val id: String,
    val title: String,
    val carImage: String,
    val price: String,
    val fuelType: UiFuelType
)

enum class UiFuelType {
    FUEL, ELECTRIC, HYBRID;

    companion object {
        fun fromFuelType(value: FuelType?): UiFuelType {
            return when (value) {
                FuelType.GASOLINE -> FUEL
                FuelType.DIESEL -> FUEL
                FuelType.HYBRID -> HYBRID
                FuelType.ELECTRIC -> ELECTRIC
                else -> FUEL
            }
        }
    }
}