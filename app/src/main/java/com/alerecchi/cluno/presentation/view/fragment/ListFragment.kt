package com.alerecchi.cluno.presentation.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alerecchi.cluno.R
import com.alerecchi.cluno.di.provider.ViewModelFactory
import com.alerecchi.cluno.presentation.model.LoadingState
import com.alerecchi.cluno.presentation.rx.EventBus
import com.alerecchi.cluno.presentation.rx.bindToLifecycle
import com.alerecchi.cluno.presentation.rx.events.ItemClickEvent
import com.alerecchi.cluno.presentation.view.recyclerview.OfferAdapter
import com.alerecchi.cluno.presentation.viewmodel.ListViewModel
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

class ListFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    lateinit var offerAdapter: OfferAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        offerAdapter = OfferAdapter()

        val viewModel = ViewModelProviders.of(this, viewModelFactory)[ListViewModel::class.java]

        carRecycler.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = offerAdapter
            addItemDecoration(DividerItemDecoration(context, RecyclerView.VERTICAL))
        }

        /* The observer is tied to the lifecycle of the Fragment's view instead of the Fragment instance itself because of the different
        lifecycle that these two component have. This is done to prevent multiple subscription and correct dispose of the reactive source
         */
        viewModel.offerList.observe(viewLifecycleOwner, Observer {
            offerAdapter.submitList(it)
        })

        viewModel.loadingState.observe(viewLifecycleOwner, Observer {
            when (it) {
                LoadingState.ERROR -> {
                    Toast.makeText(context, "Network Error Please try again later", Toast.LENGTH_LONG).show()
                    loadingBar.visibility = View.GONE
                }
                LoadingState.LOADING -> loadingBar.visibility = View.VISIBLE
                LoadingState.COMPLETE -> loadingBar.visibility = View.GONE
            }
        })

        EventBus
            .observeEvents(ItemClickEvent::class.java)
            .subscribe { event ->
                activity?.supportFragmentManager
                    ?.beginTransaction()
                    ?.replace(R.id.root, DetailFragment.newInstance(event.itemId))
                    ?.addToBackStack(null)
                    ?.commit()
            }
            .bindToLifecycle(viewLifecycleOwner)
    }
}