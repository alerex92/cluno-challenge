package com.alerecchi.cluno.presentation.view.recyclerview.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alerecchi.cluno.R
import com.alerecchi.cluno.presentation.model.UiFuelType
import com.alerecchi.cluno.presentation.model.UiOffer
import com.alerecchi.cluno.presentation.rx.EventBus
import com.alerecchi.cluno.presentation.rx.events.ItemClickEvent
import com.bumptech.glide.Glide
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_offer_item.*

class OfferViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    companion object {
        fun newInstance(parent: ViewGroup): OfferViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.view_offer_item, parent, false)
            return OfferViewHolder(view)
        }
    }

    fun bindTo(model: UiOffer) {

        offerTitle.text = model.title

        Glide
            .with(containerView)
            .load(model.carImage)
            .into(offerImage)

        offerPrice.text = model.price

        when (model.fuelType) {
            UiFuelType.FUEL -> {
                gasPump.visibility = View.VISIBLE
                electricCharger.visibility = View.INVISIBLE
            }
            UiFuelType.HYBRID -> {
                gasPump.visibility = View.VISIBLE
                electricCharger.visibility = View.VISIBLE
            }
            UiFuelType.ELECTRIC -> {
                gasPump.visibility = View.INVISIBLE
                electricCharger.visibility = View.VISIBLE
            }
        }

        itemView.setOnClickListener {
            EventBus.post(ItemClickEvent(model.id, offerImage))
        }
    }
}