package com.alerecchi.cluno.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import com.alerecchi.cluno.domain.model.Offer
import com.alerecchi.cluno.domain.repository.OfferRepository
import com.alerecchi.cluno.presentation.mapper.asUiDetailModel
import com.alerecchi.cluno.presentation.model.LoadingState
import com.alerecchi.cluno.presentation.model.UiDetail
import com.alerecchi.cluno.presentation.rx.bindToLifecycle
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class DetailViewModel @Inject constructor(private val repository: OfferRepository) : LifecycleViewModel() {

    val loadingState = MutableLiveData<LoadingState>()
    val offer = MutableLiveData<UiDetail>()

    init {
        loadingState.value = LoadingState.LOADING
    }

    fun fetchOffer(id: String) {
        repository
            .getOffer(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                processResult(it)
                loadingState.value = LoadingState.COMPLETE
            }, {
                loadingState.value = LoadingState.ERROR
            }).bindToLifecycle(this)
    }

    private fun processResult(result: Offer) {
        offer.value = result.asUiDetailModel()
    }
}