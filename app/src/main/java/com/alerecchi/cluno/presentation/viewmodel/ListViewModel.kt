package com.alerecchi.cluno.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import com.alerecchi.cluno.domain.model.Offer
import com.alerecchi.cluno.domain.repository.OfferRepository
import com.alerecchi.cluno.presentation.mapper.asUiModel
import com.alerecchi.cluno.presentation.model.LoadingState
import com.alerecchi.cluno.presentation.model.UiOffer
import com.alerecchi.cluno.presentation.rx.bindToLifecycle
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class ListViewModel @Inject constructor(repository: OfferRepository) : LifecycleViewModel() {

    val loadingState = MutableLiveData<LoadingState>()
    val offerList = MutableLiveData<List<UiOffer>>()

    /* In this call a subscribeOn it's missing because it's added automatically by the retrofit instance. Retrofit has
    been created using a "RxJava2CallAdapterFactory.createAsync()" adapter factory so every subscribeOn added manually would
    have been ignored. */
    init {
        loadingState.value = LoadingState.LOADING
        repository
            .getOffers()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                processResult(it)
                loadingState.value = LoadingState.COMPLETE
            }, {
                loadingState.value = LoadingState.ERROR
            }).bindToLifecycle(this)
    }

    private fun processResult(list: List<Offer>) {
        val uiList = list
            .filter { it.available }
            .map { it.asUiModel() }
        offerList.value = uiList
    }
}