package com.alerecchi.cluno.presentation.model

enum class LoadingState {

    ERROR, LOADING, COMPLETE
}