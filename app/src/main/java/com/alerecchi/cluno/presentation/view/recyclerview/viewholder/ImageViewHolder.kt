package com.alerecchi.cluno.presentation.view.recyclerview.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alerecchi.cluno.R
import com.bumptech.glide.Glide
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_image_item.*

class ImageViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    companion object {
        fun newInstance(parent: ViewGroup): ImageViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.view_image_item, parent, false)
            return ImageViewHolder(view)
        }
    }

    fun bindTo(model: String) {
        Glide.with(image)
            .load(model)
            .into(image)
    }
}