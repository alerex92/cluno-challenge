package com.alerecchi.cluno.presentation.rx

import com.alerecchi.cluno.presentation.rx.events.Event
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable

/**
 * A simple implementation of a global event bus, very useful for communication between components that are not directly
 * connected.
 */
object EventBus {

    private val publishRelay: PublishRelay<Event> = PublishRelay.create<Event>()

    fun post(event: Event) {
        publishRelay.accept(event)
    }

    fun <T : Event> observeEvents(eventClass: Class<T>): Observable<T> {
        return publishRelay.ofType(eventClass)
    }

}