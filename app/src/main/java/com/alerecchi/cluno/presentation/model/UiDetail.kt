package com.alerecchi.cluno.presentation.model

data class UiDetail(
    val id: String,
    val title: String,
    val carImage: String,
    val price: String,
    val images: List<String>?,
    val deliveryDetails: String
)