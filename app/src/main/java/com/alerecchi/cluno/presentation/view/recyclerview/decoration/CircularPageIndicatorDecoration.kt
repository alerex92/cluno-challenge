package com.alerecchi.cluno.presentation.view.recyclerview.decoration

import android.graphics.Canvas
import android.graphics.Paint
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class CircularPageIndicatorDecoration(
    private val dotsRadiusActivePx: Int,
    private val dotsRadiusInactivePx: Int,
    private val dotsDistancePx: Int,
    private val dotsBottomMarginPx: Int,
    private val colorActive: Int,
    private val colorInactive: Int
) : RecyclerView.ItemDecoration() {

    private var activePosition = 0
    private val paint: Paint = Paint()

    init {
        paint.isAntiAlias = true
        paint.strokeCap = Paint.Cap.ROUND
        paint.strokeWidth = 0f
        paint.style = Paint.Style.FILL_AND_STROKE
    }

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDrawOver(c, parent, state)

        val itemCount = parent.adapter!!.itemCount

        // center horizontally, calculate width and subtract half from center
        val totalLength = (dotsRadiusInactivePx * 2 * itemCount).toFloat()
        val paddingBetweenItems = (Math.max(0, itemCount - 1) * dotsDistancePx).toFloat()
        val indicatorTotalWidth = totalLength + paddingBetweenItems
        val indicatorStartX = (parent.width - indicatorTotalWidth) / 2f

        // center vertically in the allotted space
        val indicatorPosY = (parent.height - (dotsBottomMarginPx + dotsRadiusInactivePx * 2)).toFloat()

        drawInactiveIndicators(c, indicatorStartX, indicatorPosY, itemCount)

        // find active page (which should be highlighted)
        val layoutManager = parent.layoutManager as LinearLayoutManager?
        val activePosition = layoutManager!!.findFirstVisibleItemPosition()
        if (activePosition != RecyclerView.NO_POSITION) {
            drawHighlights(c, indicatorStartX, indicatorPosY, activePosition)
        }
    }

    private fun drawInactiveIndicators(c: Canvas, indicatorStartX: Float, indicatorPosY: Float, itemCount: Int) {
        paint.color = colorInactive

        // width of item indicator including padding
        val itemWidth = (dotsRadiusInactivePx * 2 + dotsDistancePx).toFloat()

        var start = indicatorStartX
        for (i in 0 until itemCount) {
            // draw the circle for every item
            c.drawCircle(
                start + dotsRadiusInactivePx,
                indicatorPosY + dotsRadiusInactivePx,
                dotsRadiusInactivePx.toFloat(),
                paint
            )
            start += itemWidth
        }
    }

    private fun drawHighlights(
        c: Canvas, indicatorStartX: Float, indicatorPosY: Float,
        highlightPosition: Int
    ) {
        paint.color = colorActive

        // width of item indicator including padding
        val itemWidth = (dotsRadiusInactivePx * 2 + dotsDistancePx).toFloat()

        val highlightStart = indicatorStartX + itemWidth * highlightPosition
        c.drawCircle(
            highlightStart + dotsRadiusInactivePx,
            indicatorPosY + dotsRadiusInactivePx,
            dotsRadiusActivePx.toFloat(),
            paint
        )
    }
}