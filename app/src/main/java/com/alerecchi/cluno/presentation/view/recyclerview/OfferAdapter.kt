package com.alerecchi.cluno.presentation.view.recyclerview

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.alerecchi.cluno.presentation.model.UiOffer
import com.alerecchi.cluno.presentation.view.recyclerview.viewholder.OfferViewHolder

class OfferAdapter : ListAdapter<UiOffer, OfferViewHolder>(ItemCallback()) {

    class ItemCallback : DiffUtil.ItemCallback<UiOffer>() {
        override fun areItemsTheSame(oldItem: UiOffer, newItem: UiOffer): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: UiOffer, newItem: UiOffer): Boolean {
            return oldItem == newItem
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferViewHolder {
        return OfferViewHolder.newInstance(parent)
    }

    override fun onBindViewHolder(holder: OfferViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }
}