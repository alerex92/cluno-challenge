package com.alerecchi.cluno.presentation.rx.events

import android.view.View

data class ItemClickEvent(
    val itemId: String,
    val imageView: View
) : Event