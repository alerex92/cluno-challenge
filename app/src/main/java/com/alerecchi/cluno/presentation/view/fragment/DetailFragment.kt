package com.alerecchi.cluno.presentation.view.fragment


import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.alerecchi.cluno.R
import com.alerecchi.cluno.di.provider.ViewModelFactory
import com.alerecchi.cluno.presentation.model.LoadingState
import com.alerecchi.cluno.presentation.view.recyclerview.ImageAdapter
import com.alerecchi.cluno.presentation.view.recyclerview.decoration.CircularPageIndicatorDecoration
import com.alerecchi.cluno.presentation.viewmodel.DetailViewModel
import com.bumptech.glide.Glide
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_detail.*
import javax.inject.Inject


class DetailFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    companion object {

        private const val EXTRA_ID = "extraId"

        fun newInstance(id: String): DetailFragment {
            val fragment = DetailFragment()
            val extras = bundleOf(EXTRA_ID to id)
            fragment.arguments = extras

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val id = arguments?.getString(EXTRA_ID) ?: ""

        val viewModel = ViewModelProviders.of(this, viewModelFactory)[DetailViewModel::class.java]

        viewModel.fetchOffer(id)

        viewModel.offer.observe(viewLifecycleOwner, Observer { model ->

            if (model.images.isNullOrEmpty()) {
                detailCarImages.visibility = View.GONE
                detailCarImage.visibility = View.VISIBLE
                Glide.with(this)
                    .load(model.carImage)
                    .into(detailCarImage)
            } else {
                detailCarImages.visibility = View.VISIBLE
                detailCarImage.visibility = View.GONE
                val imageAdapter = ImageAdapter()
                val decoration = CircularPageIndicatorDecoration(
                    resources.getDimensionPixelSize(R.dimen.dotRadiusActive),
                    resources.getDimensionPixelSize(R.dimen.dotRadiusInactive),
                    resources.getDimensionPixelSize(R.dimen.dotsDistance),
                    resources.getDimensionPixelSize(R.dimen.dotsBottomMargins),
                    Color.BLACK,
                    Color.parseColor("#4D000000")
                )
                detailCarImages.apply {
                    layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
                    adapter = imageAdapter
                    PagerSnapHelper().attachToRecyclerView(this)
                    addItemDecoration(decoration)
                }
                imageAdapter.submitList(model.images)
            }

            detailCarName.text = model.title
            detailDelivery.text = model.deliveryDetails
            detailPrice.text = model.price
        })

        viewModel.loadingState.observe(viewLifecycleOwner, Observer {
            when (it) {
                LoadingState.ERROR -> {
                    Toast.makeText(context, "Network Error Please try again later", Toast.LENGTH_LONG).show()
                    loadingBar.visibility = View.GONE
                }
                LoadingState.LOADING -> loadingBar.visibility = View.VISIBLE
                LoadingState.COMPLETE -> loadingBar.visibility = View.GONE
            }
        })


    }
}