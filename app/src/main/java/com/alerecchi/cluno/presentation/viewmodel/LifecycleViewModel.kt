package com.alerecchi.cluno.presentation.viewmodel

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.ViewModel

/**
 * This abstract viewmodel implements a lifecycle so the disposables used in child classes can take advantage of the
 * bindToLifecycle extension
 */
abstract class LifecycleViewModel : ViewModel(), LifecycleOwner {

    @Suppress("LeakingThis") //LifecycleRegistry holds a weak reference of the class
    private val lifecycleRegistry = LifecycleRegistry(this)

    init {
        lifecycleRegistry.markState(Lifecycle.State.CREATED)
    }

    override fun onCleared() {
        super.onCleared()
        lifecycleRegistry.markState(Lifecycle.State.DESTROYED)
    }

    override fun getLifecycle() = lifecycleRegistry
}