package com.alerecchi.cluno.presentation.mapper

import com.alerecchi.cluno.domain.model.Offer
import com.alerecchi.cluno.presentation.model.UiDetail
import com.alerecchi.cluno.presentation.model.UiFuelType
import com.alerecchi.cluno.presentation.model.UiOffer

fun Offer.asUiModel(): UiOffer {
    return UiOffer(
        id = id,
        title = teaser?.title ?: "",
        carImage = teaser?.image ?: "",
        price = pricing?.let {
            it.value.toInt().toString() + it.symbol
        } ?: "",
        fuelType = UiFuelType.fromFuelType(car?.fuelType)
    )
}

fun Offer.asUiDetailModel(): UiDetail {
    return UiDetail(
        id = id,
        title = teaser?.title ?: "",
        carImage = teaser?.image ?: "",
        price = pricing?.let {
            it.value.toInt().toString() + it.symbol
        } ?: "",
        images = images,
        deliveryDetails = estimatedDelivery
    )
}