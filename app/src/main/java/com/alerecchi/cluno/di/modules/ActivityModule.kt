package com.alerecchi.cluno.di.modules

import com.alerecchi.cluno.di.scope.ActivityScoped
import com.alerecchi.cluno.presentation.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun mainActivity(): MainActivity

}