package com.alerecchi.cluno.di.modules

import com.alerecchi.cluno.data.OfferRepositoryImpl
import com.alerecchi.cluno.data.datasource.OfferDataSource
import com.alerecchi.cluno.data.datasource.OfferRemoteDataSource
import com.alerecchi.cluno.domain.repository.OfferRepository
import dagger.Binds
import dagger.Module

@Module
abstract class DataModule {

    @Binds
    abstract fun bindOfferRemoteDataSource(offerDataSource: OfferRemoteDataSource): OfferDataSource

    @Binds
    abstract fun bindOfferRepository(offerRepository: OfferRepositoryImpl): OfferRepository
}