package com.alerecchi.cluno.di.provider

import androidx.lifecycle.ViewModel
import dagger.MapKey
import kotlin.reflect.KClass

/**
 * Created by Giuseppe Buzzanca on 25/01/2018
 * giuseppe.buzzanca@accenture.com
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)