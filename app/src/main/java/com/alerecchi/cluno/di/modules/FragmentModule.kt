package com.alerecchi.cluno.di.modules

import com.alerecchi.cluno.di.scope.FragmentScoped
import com.alerecchi.cluno.presentation.view.fragment.DetailFragment
import com.alerecchi.cluno.presentation.view.fragment.ListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun listFragment(): ListFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun detailFragment(): DetailFragment
}