package com.alerecchi.cluno.data.network.model

data class NetworkEmissionLabelChart(
    val png: String?,
    val svg: String?
)