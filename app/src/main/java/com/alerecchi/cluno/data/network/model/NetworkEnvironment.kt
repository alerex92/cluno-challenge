package com.alerecchi.cluno.data.network.model

data class NetworkEnvironment(
    val consumptionCity: Double,
    val consumptionCombined: Double,
    val consumptionCountry: Double,
    val emissionClass: String?,
    val emissionCO2: Int,
    val emissionLabel: String?,
    val emissionLabelChart: NetworkEmissionLabelChart?,
    val underline: String?
)