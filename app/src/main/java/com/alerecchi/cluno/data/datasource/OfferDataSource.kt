package com.alerecchi.cluno.data.datasource

import com.alerecchi.cluno.domain.model.Offer
import io.reactivex.Single

interface OfferDataSource {

    fun getOffers(): Single<List<Offer>>

    fun getOffer(id: String): Single<Offer>
}