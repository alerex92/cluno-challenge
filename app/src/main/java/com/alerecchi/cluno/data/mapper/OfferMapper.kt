package com.alerecchi.cluno.data.mapper

import com.alerecchi.cluno.data.network.model.NetworkOffer
import com.alerecchi.cluno.domain.model.Offer

fun NetworkOffer.asDomainModel(): Offer {
    return Offer(
        id = id,
        available = available,
        segment = segment ?: "",
        detailUrl = detailUrl!!,
        estimatedDelivery = estimatedDeliveryTime ?: "",
        car = car?.asDomainModel(),
        images = images?.map { it.src ?: "" },
        teaser = teaser?.asDomainModel(),
        labels = labels ?: listOf(),
        pricing = pricing?.asDomainModel()
    )
}