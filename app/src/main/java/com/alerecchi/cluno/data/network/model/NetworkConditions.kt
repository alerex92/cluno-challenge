package com.alerecchi.cluno.data.network.model

data class NetworkConditions(
    val minimumAge: Int,
    val maximumAge: Int,
    val minLicenseDuration: Int
)