package com.alerecchi.cluno.data.network.service

import com.alerecchi.cluno.data.network.model.NetworkOffer
import com.alerecchi.cluno.data.network.model.NetworkOffers
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface OfferService {

    @GET("offerservice/v1/offer/query")
    fun getOffers(): Single<NetworkOffers>

    @GET("offerservice/v1/offer/{id}")
    fun getOffer(@Path("id") id: String): Single<NetworkOffer>
}