package com.alerecchi.cluno.data

import com.alerecchi.cluno.data.datasource.OfferDataSource
import com.alerecchi.cluno.domain.repository.OfferRepository
import javax.inject.Inject

/**
 * A repository is used to abstract orchestration of multiple data sources. In this case is not entirely needed but it has
 * been added to show the full Repository pattern usually used in more complex applications.
 */
class OfferRepositoryImpl @Inject constructor(private val dataSource: OfferDataSource) : OfferRepository {

    override fun getOffers() = dataSource.getOffers()

    override fun getOffer(id: String) = dataSource.getOffer(id)
}