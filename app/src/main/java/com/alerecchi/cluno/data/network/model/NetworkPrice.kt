package com.alerecchi.cluno.data.network.model

data class NetworkPrice(
    val price: Int,
    val startingFee: Int,
    val deliveryFee: Int,
    val note: String?,
    val underline: String?,
    val term: String?,
    val currencyIsoCode: String?,
    val currencySymbol: String?,
    val excessKilometers: Double,
    val monthlyExcessKilometers: Int,
    val unusedKilometers: Int,
    val includedAnnualKilometers: Int,
    val deductiblePartialCover: Int,
    val deductibleFullyComprehensive: Int,
    val bookableOptions: List<NetworkBookableOption>?


)