package com.alerecchi.cluno.data.network.model

data class NetworkOffer(
    val id: String,
    val available: Boolean,
    val portfolio: String?,
    val segment: String?,
    val detailUrl: String?,
    val estimatedDeliveryTime: String?,
    val car: NetworkCar?,
    val teaser: NetworkTeaser?,
    val images: List<NetworkImage>?,
    val labels: List<String>?,
    val conditions: NetworkConditions?,
    val pricing: NetworkPrice?
)