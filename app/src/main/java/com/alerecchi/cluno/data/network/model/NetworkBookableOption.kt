package com.alerecchi.cluno.data.network.model

data class NetworkBookableOption(
    val name: String?,
    val price: Int,
    val selected: Boolean
)