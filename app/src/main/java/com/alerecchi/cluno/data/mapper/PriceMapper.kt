package com.alerecchi.cluno.data.mapper

import com.alerecchi.cluno.data.network.model.NetworkPrice
import com.alerecchi.cluno.domain.model.Price

fun NetworkPrice.asDomainModel(): Price {
    return Price(
        value = price.toFloat(),
        symbol = currencySymbol ?: "",
        isoCode = currencyIsoCode ?: ""
    )
}