package com.alerecchi.cluno.data.network.model

data class NetworkTeaser(
    val title: String?,
    val subtitle: String?,
    val ribbon: String?,
    val teaserImage: String?,
    val equipmentHighlights: List<String>?
)