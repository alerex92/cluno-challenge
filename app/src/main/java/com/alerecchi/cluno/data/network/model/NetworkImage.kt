package com.alerecchi.cluno.data.network.model

data class NetworkImage(
    val height: Int,
    val src: String?,
    val title: String?,
    val width: Int
)