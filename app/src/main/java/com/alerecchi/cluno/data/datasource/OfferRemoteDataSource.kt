package com.alerecchi.cluno.data.datasource

import com.alerecchi.cluno.data.mapper.asDomainModel
import com.alerecchi.cluno.data.network.service.OfferService
import com.alerecchi.cluno.domain.model.Offer
import io.reactivex.Single
import javax.inject.Inject

/**
 * A data source is an abstraction on a source of data so that implementation details are hidden from the user of this source.
 * In this case the retrofit implementation of the API is abstracted by this class.
 */
class OfferRemoteDataSource @Inject constructor(private val service: OfferService) :
    OfferDataSource {

    override fun getOffers(): Single<List<Offer>> {
        return service
            .getOffers()
            .map { response -> response.items?.map { it.asDomainModel() } }
    }

    override fun getOffer(id: String): Single<Offer> {
        return service
            .getOffer(id)
            .map { response -> response.asDomainModel() }
    }
}