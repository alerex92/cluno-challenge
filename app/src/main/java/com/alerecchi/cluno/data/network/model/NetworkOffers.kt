package com.alerecchi.cluno.data.network.model

data class NetworkOffers(
    val count: Int,
    val items: List<NetworkOffer>?
)