package com.alerecchi.cluno.data.network.model

data class NetworkCar(
    val make: String?,
    val model: String?,
    val version: String?,
    val engine: String?,
    val kw: Int,
    val ps: Int,
    val doors: String?,
    val drive: String?,
    val ccm: String?,
    val fueltype: String?,
    val gearingType: String?,
    val offerExtColor: String?,
    val equipmentDetails: List<String>?,
    val environment: NetworkEnvironment?
)