package com.alerecchi.cluno.data.mapper

import com.alerecchi.cluno.data.network.model.NetworkTeaser
import com.alerecchi.cluno.domain.model.Teaser

fun NetworkTeaser.asDomainModel(): Teaser {
    return Teaser(
        title = title ?: "",
        subtitle = subtitle ?: "",
        ribbon = ribbon ?: "",
        image = teaserImage ?: "",
        equipmentHighlights = equipmentHighlights ?: listOf()
    )
}