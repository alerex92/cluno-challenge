package com.alerecchi.cluno.data.mapper

import com.alerecchi.cluno.data.network.model.NetworkCar
import com.alerecchi.cluno.domain.model.Car
import com.alerecchi.cluno.domain.model.FuelType
import com.alerecchi.cluno.domain.model.GearType

/**
 * A mapper is an helper class / function that has the task of transforming data to better suit the layer in which the data will
 * used. In this case the mapper is converting the network data in a domain format (in the domain layer only the "purest" version
 * of models is allowed)
 */
fun NetworkCar.asDomainModel(): Car {
    return Car(
        make = make ?: "",
        model = model ?: "",
        kiloWat = kw,
        horsePower = ps,
        version = version ?: "",
        gearingType = GearType.fromString(gearingType),
        fuelType = FuelType.fromString(fueltype)
    )
}