package com.alerecchi.cluno.domain.model

data class Price(
    val value: Float,
    val symbol: String,
    val isoCode: String
)