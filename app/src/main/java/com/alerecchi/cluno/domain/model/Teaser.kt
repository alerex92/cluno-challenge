package com.alerecchi.cluno.domain.model

data class Teaser(
    val title: String,
    val subtitle: String,
    val ribbon: String,
    val image: String,
    val equipmentHighlights: List<String>
)