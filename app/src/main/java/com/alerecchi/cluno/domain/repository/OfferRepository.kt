package com.alerecchi.cluno.domain.repository

import com.alerecchi.cluno.domain.model.Offer
import io.reactivex.Single

interface OfferRepository {

    fun getOffers(): Single<List<Offer>>

    fun getOffer(id: String): Single<Offer>
}