package com.alerecchi.cluno.domain.model

data class Offer(
    val id: String,
    val available: Boolean,
    val segment: String,
    val detailUrl: String,
    val estimatedDelivery: String,
    val car: Car?,
    val teaser: Teaser?,
    val images: List<String>?,
    val labels: List<String>,
    val pricing: Price?
)