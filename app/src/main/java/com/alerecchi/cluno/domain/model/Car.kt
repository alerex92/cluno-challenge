package com.alerecchi.cluno.domain.model

data class Car(
    val make: String,
    val model: String,
    val kiloWat: Int,
    val horsePower: Int,
    val version: String,
    val gearingType: GearType,
    val fuelType: FuelType
)