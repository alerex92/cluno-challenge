package com.alerecchi.cluno.domain.model

enum class FuelType {
    GASOLINE, DIESEL, HYBRID, ELECTRIC;

    companion object {
        fun fromString(value: String?): FuelType {
            return when (value) {
                "Benzin" -> GASOLINE
                "Diesel" -> DIESEL
                "Elektrisch/Benzin" -> HYBRID
                "Elektrisch" -> ELECTRIC
                else -> GASOLINE
            }
        }
    }
}