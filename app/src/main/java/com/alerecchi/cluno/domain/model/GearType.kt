package com.alerecchi.cluno.domain.model

enum class GearType {
    MANUAL, AUTOMATIC;

    companion object {
        fun fromString(value: String?): GearType {
            return when (value) {
                "Schaltgetriebe" -> MANUAL
                "Automatik" -> AUTOMATIC
                else -> MANUAL
            }
        }
    }
}