package com.alerecchi.cluno.data.datasource

import com.alerecchi.cluno.data.OfferRepositoryImpl
import com.alerecchi.cluno.di.modules.NetworkModule
import com.alerecchi.cluno.domain.model.*
import com.alerecchi.cluno.util.JsonUtils
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Test

/**
 * Example of an integration test of all the data layer. With these kinds of test we can find issues with the logic inside
 * the data layer components (in this case mappers are the only logic in this layer) and we could test different kinds
 * of responses, both successful and unsuccessful
 */
class OfferRemoteDataSourceTest {

    private val netModule = NetworkModule()
    private val mockWebServer = MockWebServer()
    private val retrofit = netModule.providesRetrofit(OkHttpClient(), Gson())
    private val service = netModule.providesOfferService(retrofit)
    private val dataSource = OfferRemoteDataSource(service)
    private val repository = OfferRepositoryImpl(dataSource)

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }


    @Test
    fun getOffer() {
        val expectedModel = Offer(
            id = "115",
            available = true,
            segment = "1B",
            detailUrl = "/de/portfolio/opel-corsa-edition-115/",
            estimatedDelivery = "",
            car = Car(
                make = "Opel",
                model = "Corsa",
                kiloWat = 66,
                horsePower = 90,
                version = "EDITION",
                gearingType = GearType.fromString("Schaltgetriebe"),
                fuelType = FuelType.GASOLINE
            ),
            images = null,
            teaser = Teaser(
                title = "Opel Corsa",
                subtitle = "Enthält alles außer Tanken",
                ribbon = "",
                image = "https://assets.cluno.com/2019/02/115_1_black.jpg",
                equipmentHighlights = listOf(
                    "Einparkhilfe (v/h)",
                    "Navi via App",
                    "Sitzheizung"
                )
            ),
            labels = listOf(),
            pricing = Price(259f, "€", "EUR")
        )
        mockWebServer.enqueue(MockResponse().setResponseCode(200).setBody(JsonUtils.getJson("json/offers.json")))

        val result = repository.getOffers().blockingGet()

        assertThat(result).isNotNull
        assertThat(result).hasSize(50)
        assertThat(result[0]).isEqualTo(expectedModel)
    }
}