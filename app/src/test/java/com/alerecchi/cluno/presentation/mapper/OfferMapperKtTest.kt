package com.alerecchi.cluno.presentation.mapper

import com.alerecchi.cluno.domain.model.*
import com.alerecchi.cluno.presentation.model.UiFuelType
import com.alerecchi.cluno.presentation.model.UiOffer
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

/**
 * A unit test for the UI mapper of the list item model.
 */
class OfferMapperKtTest {

    @Test
    fun asUiModel() {
        val domainModel = Offer(
            id = "115",
            available = true,
            segment = "1B",
            detailUrl = "/de/portfolio/opel-corsa-edition-115/",
            estimatedDelivery = "",
            car = Car(
                make = "Opel",
                model = "Corsa",
                kiloWat = 66,
                horsePower = 90,
                version = "EDITION",
                gearingType = GearType.fromString("Schaltgetriebe"),
                fuelType = FuelType.GASOLINE
            ),
            images = null,
            teaser = Teaser(
                title = "Opel Corsa",
                subtitle = "Enthält alles außer Tanken",
                ribbon = "",
                image = "https://assets.cluno.com/2019/02/115_1_black.jpg",
                equipmentHighlights = listOf(
                    "Einparkhilfe (v/h)",
                    "Navi via App",
                    "Sitzheizung"
                )
            ),
            labels = listOf(),
            pricing = Price(259f, "€", "EUR")
        )

        val uiModel = UiOffer(
            id = "115",
            title = "Opel Corsa",
            price = "259€",
            carImage = "https://assets.cluno.com/2019/02/115_1_black.jpg",
            fuelType = UiFuelType.FUEL
        )

        assertThat(domainModel.asUiModel()).isEqualTo(uiModel)
    }
}